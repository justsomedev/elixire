# elixire: Image Host software
# Copyright 2018-2019, elixi.re Team and the elixire contributors
# SPDX-License-Identifier: AGPL-3.0-only

"""
elixi.re - email functions
"""
import secrets
import logging
from typing import Tuple, Optional
from collections import namedtuple
from dataclasses import dataclass
from enum import Enum, auto

import aiohttp
from quart import current_app as app

from api.errors import BadInput
from api.common.email.templates import render

log = logging.getLogger(__name__)
Error = namedtuple("Error", "status")


class EmailTokenType(Enum):
    password_reset = auto()
    account_deletion = auto()
    account_activation = auto()
    datadump_result = auto()

    @property
    def table_name(self) -> str:
        return {
            self.password_reset: "email_pwd_reset_tokens",
            self.account_deletion: "email_deletion_tokens",
            self.account_activation: "email_activation_tokens",
            self.datadump_result: "email_dump_tokens",
        }[self]


async def make_email_token(user_id, token_type: EmailTokenType, count: int = 0) -> str:
    """Generate a secret token intended for triggering potentially destructive/dangerous actions via email, inserting it into relevant database.

    Calls the database to give an unique token.

    Parameters
    ----------
    app: sanic.App
        Application instance for database access.
    user_id: int
        User snowflake ID.
    table: str
        The table to be used for checking.

    Returns
    -------
    str
        The email token to be used.

    Raises
    ------
    BadInput
        When the funcion entered more than 10 retries,
        or there are more than 3 tokens issued in the span
        of a time window (defined by the table)
    """

    table_name = token_type.table_name

    if count == 11:
        # it really shouldn't happen,
        # but we better be ready for it.
        raise BadInput("Failed to generate an email hash.")

    possible_token = secrets.token_hex(32)

    # check if hash already exists
    other_id = await app.db.fetchval(
        f"""
    SELECT user_id
    FROM {table_name}
    WHERE hash = $1 AND now() < expiral
    """,
        possible_token,
    )

    if other_id:
        # retry with count + 1
        return await make_email_token(user_id, token_type, count=count + 1)

    hashes = await app.db.fetchval(
        f"""
    SELECT COUNT(*)
    FROM {table_name}
    WHERE user_id = $1 AND now() < expiral
    """,
        user_id,
    )

    if hashes > 3:
        raise BadInput("You already generated more than 3 tokens in the time period.")

    log.info("generated email token %r for type %r", possible_token, token_type)

    await app.db.execute(
        f"""
    INSERT INTO {table_name} (hash, user_id)
    VALUES ($1, $2)
    """,
        possible_token,
        user_id,
    )

    return possible_token


def fmt_email(string, **kwargs):
    """Format an email"""
    base = {
        "inst_name": app.econfig.INSTANCE_NAME,
        "support": app.econfig.SUPPORT_EMAIL,
        "main_url": app.econfig.MAIN_URL,
        "main_invite": app.econfig.MAIN_INVITE,
    }

    base.update(kwargs)
    log.debug("formatting %r", string)
    return string.replace("{}", "{{}}").format(**base)


async def uid_from_email_token(token: str, table: str, raise_err: bool = True) -> int:
    """Get user ID from email token."""
    user_id = await app.db.fetchval(
        f"""
    SELECT user_id
    FROM {table}
    WHERE hash=$1
    """,
        token,
    )

    if not user_id and raise_err:
        raise BadInput("No user found with the token")

    return user_id


async def clean_etoken(token: str, table: str) -> bool:
    res = await app.db.execute(
        f"""
    DELETE FROM {table}
    WHERE hash=$1
    """,
        token,
    )

    return res == "DELETE 1"


class EmailError(Exception):
    pass


@dataclass
class MailgunSender:
    async def send(self, email: str, subject: str, body: str) -> None:
        econfig = app.econfig
        mailgun_url = f"https://api.mailgun.net/v3/{econfig.MAILGUN_DOMAIN}/messages"
        auth = aiohttp.BasicAuth("api", econfig.MAILGUN_API_KEY)
        data = {
            "from": f"{econfig.INSTANCE_NAME} <automated@{econfig.MAILGUN_DOMAIN}>",
            "to": [email],
            "subject": subject,
            "text": body,
        }

        async with app.session.post(mailgun_url, auth=auth, data=data) as resp:
            if resp.status != 200:
                respbody = await resp.read()
                raise EmailError(f"Failed to send email. {resp.status} {respbody}")

            log.info(
                "sent %d bytes email, subject=%r",
                len(body),
                subject,
            )


# TODO move email token interfaces to EmailManager
# TODO create WebhookManager with the same style
class EmailManager:
    def __init__(self, sender):
        self.sender = sender

    async def send_to_email(
        self,
        email: str,
        *,
        template: Optional[dict] = None,
        args: Optional[dict] = None,
        direct: Optional[Tuple[str, str]] = None,
    ):
        if template:
            args = args or {}
            subject, body = render(template, args)
        elif direct:
            subject, body = direct
        else:
            raise RuntimeError("No template or direct subject+body tuple provided")
        # TODO retry on email failure (job queue)
        # TODO replace MailgunSender with SMTPSender
        await self.sender.send(email, subject, body)

    async def send_to_user(
        self,
        user_id: int,
        *,
        template: Optional[dict] = None,
        args: Optional[dict] = None,
        direct: Optional[Tuple[str, str]] = None,
    ) -> str:
        args = args or {}
        user_email = await app.db.fetchval(
            """
            SELECT email
            FROM users
            WHERE user_id = $1
            """,
            user_id,
        )
        if user_email is None:
            raise AssertionError("User ID not found")

        await self.send_to_email(
            user_email, template=template, args=args, direct=direct
        )
        return user_email
