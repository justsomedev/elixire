# elixire: Image Host software
# Copyright 2018-2019, elixi.re Team and the elixire contributors
# SPDX-License-Identifier: AGPL-3.0-only

from quart import current_app as app
from typing import Tuple

PASSWORD_RESET = {
    "subject": "{_inst_name} - password reset request",
    "body": """This is an automated email from {instance_name}
about your password reset.

Please visit {main_url}/password_reset.html#{token} to
reset your password.

The link will be invalid in 30 minutes. Do not share the link with anyone else.
Nobody from support will ask you for this link.

Reply to {support} if you have any questions.

Do not reply to this email specifically, it will not work.

- {instance_name}, {main_url}
""",
}

EMAIL_DELETION = {
    "subject": "{instance_name} - account deactivation request",
    "body": """This is an automated email from {instance_name}
about your account deletion.

Please visit {main_url}/deleteconfirm.html#{token} to
confirm the deletion of your account.

The link will be invalid in 12 hours. Do not share it with anyone.

Reply to {support} if you have any questions.

If you did not make this request, email {support} since your account
might be compromised.

Do not reply to this email specifically, it will not work.

- {instance_name}, {main_url}""",
}

REGISTRATION_TEMPLATE = {
    "subject": "{instance_name} - signup confirmation",
    "body": """This is an automated email from {instance_name}
about your signup.

It has been successfully dispatched to the system so that admins can
activate the account. You will not be able to login until the account
is activated.

Accounts that aren't on the discord server won't be activated.
{main_invite}

Please do not re-register the account. It will just decrease your chances
of actually getting an account activated.

Reply to {support} if you have any questions.
Do not reply to this email specifically, it will not work.

 - {instance_name}, {main_url}
""",
}

USERNAME_RECOVERY_TEMPLATE = {
    "subject": "{inst_name} - username recovery",
    "body": """
This is an automated email from {inst_name} about
your username recovery.

Your username is {uname}.

 - {inst_name}, {main_url}
""",
}

DATADUMP_COMPLETION_TEMPLATE = {
    "subject": "{instance_name} - your data dump is here!",
    "body": """This is an automated email from {instance_name}
about your data dump.

Visit {main_url}/api/dump_get?key={dump_token} to fetch your
data dump.

The URL will be invalid in 6 hours.
Do not share this URL. Nobody will ask you for this URL.

Send an email to {support} if any questions arise.
Do not reply to this automated email.

- {instance_name}, {main_url}
    """,
}

ACCOUNT_ACTIVATION_TEMPLATE = {
    "subject": "{instance_name} - your account is now active!",
    "body": """This is an automated email from {instance_name}
about your account request.

Your account has been activated and you can now log in
at {main_url}/login.html.

Welcome to {instance_name}!

Send an email to {support} if any questions arise.
Do not reply to this automated email.

- {instance_name}, {main_url}
    """,
}

ACCOUNT_APPROVAL_TEMPLATE = {
    "subject": "{instance_name} - account activation",
    "body": """
This is an automated email from {instance_name}
about your account activation.

An administrator confirmed your account for proper activation
and you can activate your account at {main_url}/api/activate_email?key={activation_token}

You only need to use this URL once. Other attempts at using
the URL will give you an error.

Welcome to {instance_name}!

Send an email to {support} if any questions arise.
Do not reply to this automated email.

 - {instance_name}, {main_url}
""",
}


def render(template: str, args: dict) -> Tuple[str, str]:
    default_args = {
        "instance_name": app.econfig.INSTANCE_NAME,
        "support": app.econfig.SUPPORT_EMAIL,
        "main_url": app.econfig.MAIN_URL,
        "main_invite": app.econfig.MAIN_INVITE,
    }
    subject, body = (
        s.format(**{**args, **default_args})
        for s in (template["subject"], template["body"])
    )
    return subject, body
