# elixire: Image Host software
# Copyright 2018-2019, elixi.re Team and the elixire contributors
# SPDX-License-Identifier: AGPL-3.0-only

"""
elixi.re backend source code - register route

This also includes routes like recovering username from email.
"""

import asyncpg

from quart import Blueprint, jsonify, current_app as app, request
from dns import resolver

from ..errors import BadInput, FeatureDisabled
from ..schema import validate, REGISTRATION_SCHEMA, RECOVER_USERNAME
from ..common.webhook import register_webhook
from api.common.email.templates import REGISTRATION_TEMPLATE, USERNAME_RECOVERY_TEMPLATE
from api.common.user import create_user

bp = Blueprint("register", __name__)


async def check_email(email: str):
    """Check if a given email has an MX record.

    This does not check if the result of the MX record query
    points to a server that handles actual email.
    """
    _, domain = email.split("@")

    try:
        # check dns, MX record
        await app.loop.run_in_executor(None, app.resolv.query, domain, "MX")
    except (resolver.Timeout, resolver.NXDOMAIN, resolver.NoAnswer):
        raise BadInput("Email domain resolution failed" "(timeout or does not exist)")


@bp.post("/register")
async def register_user():
    """Send an 'account registration request' to a certain
    discord webhook.

    Look into /api/admin/activate for registration acceptance.
    """
    if not app.econfig.REGISTRATIONS_ENABLED:
        raise FeatureDisabled("Registrations are currently disabled")

    payload = validate(await request.get_json(), REGISTRATION_SCHEMA)

    username = payload["username"].lower()
    password = payload["password"]
    discord_user = payload["discord_user"]
    email = payload["email"]

    await check_email(email)

    requires_approvals = app.econfig.REQUIRE_ACCOUNT_APPROVALS

    try:
        user = await create_user(
            username=username,
            password=password,
            email=email,
            active=not requires_approvals,
        )
    except asyncpg.exceptions.UniqueViolationError:
        raise BadInput("Username or email already exist.")

    user_id = user["user_id"]

    # invalidate if anything happened before
    # just to make sure.
    await app.storage.raw_invalidate(f"uid:{username}")

    await app.email.send_to_user(user_id, template=REGISTRATION_TEMPLATE)
    succ_wb = await register_webhook(
        app.econfig.USER_REGISTER_WEBHOOK, user_id, username, discord_user, email
    )

    return jsonify(
        {
            "user_id": str(user_id),
            "success": succ_wb,
        }
    )


@bp.post("/recover_username")
async def recover_username():
    payload = validate(await request.get_json(), RECOVER_USERNAME)

    email = payload["email"]

    row = await app.db.fetchrow(
        """
    SELECT user_id, username 
    FROM users
    WHERE email = $1
    LIMIT 1
    """,
        email,
    )

    if row is None:
        raise BadInput("Email not found")

    await app.email.send_to_user(
        row["user_id"],
        template=USERNAME_RECOVERY_TEMPLATE,
        args={"username": row["username"]},
    )

    return jsonify({"success": True})
